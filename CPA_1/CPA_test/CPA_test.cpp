// CPA_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iomanip>

int main()
{
	//whenever num start with 0 it's octal
	int octal = 0123;
	//whenever num start with 0x it's hex
	int hex = 0x123;

	double num = .4 - 4.; //0.4 - 4.0

	//scientyfic notation
	int really_big_num = 3E8; //3*10^8 //it could be either small e
	double planck_constant = 6.62607e-34; //6.62607 * 10^-34

	/*Operation : ++Variable  --Variable

		Effect : Increment / decrement the variable by 1 and use its value already increased / reduced.



	Operation: Variable++  Variable--

		Effect : Use the original(unchanged) variable's value and then increment/decrement the variable by 1. */
	
	int i = 1, j;
	j = i++;
	std::cout << j << std::endl;

	std::cout << "123" << std::endl;
	char a = 'A';
	//std::cout << static_cast<char>(static_cast<int>(a)) << std::endl;
	std::cout << a + ' ' << std::endl; //convert always to num any operation on char

	int byte = 255;
	//any of hex, oct etc. are continue untill you change to other
	std::cout << "Byte in hex:" << std::hex << byte << std::endl; //ff
	std::cout << byte << std::dec << byte << std::dec << std::endl; //ff255
	std::cout << std::setbase(16) << byte << std::endl; //ff

	//can set base only for 8, 10, 16 
	std::cout << std::setbase(4) << byte << std::endl; //255

	float x = 2.5, y = 0.0000000025;
	std::cout << std::fixed << x << " " << y << std::endl; //size of first and second float are the same
	std::cout << std::scientific << x << " " << y << std::endl;


	// int / int = int
	// int / float = float
	int k;
	i = 10;
	j = 3;
	float t = (i % j * static_cast<float>(i) / 3);
	float b = (j % i - j / i);
	k = t / b;
	std::cout << k;
}
