// CPA_2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iomanip>

void and_or_xor(short left, short right) {
	std::cout << left << " " << right << std::endl;
	std::cout << "and(&): " << (left & right) << std::endl;
	std::cout << "or(&): " << (left | right) << std::endl;
	std::cout << "xor(^): " << (left ^ right) << std::endl;
}

int main()
{
	auto x = 1234L; //long int
	int y = 12345l; //int

	//numerical anomalies
	float an = 11111111000.0 + 0.00011111111;
	std::cout << std::fixed << an << std::endl;

	//binary numbers
	and_or_xor(0, 0);
	and_or_xor(1, 0);
	and_or_xor(0, 1);
	and_or_xor(1, 1);

	short a = 3; //00000011
	short b = 7; //00000111

	and_or_xor(a, b); //3 7 4
	std::cout << "negation: ~ " << ~a << std::endl; //11111100

	//shifting <<, >>
	int Signed = -8, VarS;

	unsigned Unsigned = 6, VarU;

	VarS = Signed >> 1; //division by 2 -> -4 //11000 -> 1100
	VarS = Signed << 2; //multiplication by 4 = -32 //110000
	VarU = Unsigned >> 2; //division by 4 = 1 //0110 ->0001
	VarU = Unsigned << 1; //multiplication by 2 = 2 //0001 -> 0010
}