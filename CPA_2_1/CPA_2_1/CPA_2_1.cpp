#include <iostream>

struct DATE {
	int year;
	int month;
	int day;
} dayOfBirth;

struct {
	int year;
	int month;
	int day;
} the_date_of_the_end_of_the_world;

struct Student {
	std::string name;
	DATE date_of_birth;
};

using namespace std;

int main()
{
	//<unnamed> the_date_of_the_end_of_the_world
	decltype(the_date_of_the_end_of_the_world) x;

	//inicjalizing structure	
	DATE date = { 1998, 11, 13 };

	Student student = { "Mariusz", { 1998, 11, 13 }}; // = { "Mariusz", date }
	//in structure if you do not inicjalize them with value they will be emmpty or 0 with numbers

	int *ptr;
	int num = 13;
	ptr = &num;

	//address of num
	std::cout << ptr << std::endl;
	//value of num
	std::cout << *ptr << std::endl;
	//address of ptr
	std::cout << &ptr << std::endl;

	cout << "This computing environment uses:" << endl;
	cout << sizeof(char) << " bytes for chars" << endl;
	cout << sizeof(short int) << " bytes for shorts" << endl;
	cout << sizeof(int) << " bytes for ints" << endl;
	cout << sizeof(long int) << " bytes for longs" << endl;
	cout << sizeof(float) << " bytes for floats" << endl;
	cout << sizeof(double) << " bytes for doubles" << endl;
	cout << sizeof(bool) << " byte for bools" << endl;
	cout << sizeof(int*) << " bytes for pointers" << endl;

	//void pointer can have address of any type of object but you cannot use it
	void *without_type_ptr;
	without_type_ptr = ptr;
	int* ptr1 = static_cast<int*>(without_type_ptr);
	std::cout << *ptr1 << std::endl;

	float* array = new float[20];
	delete array;

	int* count = new int;
	delete count;

	//pointer to array pointer
	int** ptrarr = new int * [10];

	for (int i = 0; i < 10; i++)
		ptrarr[i] = new int[i + 1];

	ptrarr[1][0] = 12;
	std::cout << ptrarr[1][0] << std::endl;

	delete ptrarr[1];
}