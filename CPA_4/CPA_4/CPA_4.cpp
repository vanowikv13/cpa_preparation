#include <iostream>
#include <string>

int main()
{
    //substr(substring_start_position, length_of_substring)
	std::string str1, str2;
	str1 = "ABCDEF";
	//from one one character / from 4 all character / copy all
	str2 = str1.substr(1, 1) + str1.substr(4) + str1.substr();
	std::cout << str2 << std::endl;

	//compare(substr_start, substr_length, other_string,  other_substr_start, other_substr_length)
	std::string S = "ABC";
	std::cout << S.compare(1, 1, "BC") + S.compare(2, 1, S, 2, 2) << std::endl;

	//insert(start, lenght of character, character)
	S.insert(0, 1, 'x');
	std::cout << S << std::endl;

	//assign(index, value) - sum num of value as a string
	S.assign(3, '1');
	std::cout << S << std::endl;

	//replace(start_your, end_your, string_to_replace, start_replace, end_replace)
	std::string ToDo = "I'll think about that in one hour";
	std::string Schedule = "today yesterday tomorrow";

	ToDo.replace(22, 12, Schedule, 16, 8);
	std::cout << ToDo << std::endl;

	//erase(start, num of characters to delete) - erase all or some characters from string
	ToDo.erase(0, 10);
	std::cout << ToDo << std::endl;

	//s.swap(string) swap both strings
}

/*
Promotions:
Similarly, when a float meets a double in the same expression, the float will be promoted to a double.

Formally, all the promotions are conducted according to the following set of rules (the rules are applied in the order below until all the data used in a particular expression has the same type � this condition is very important!):

    data of type char or short int will be converted to type int (this is called an integer promotion);

    data of type float undergoes a conversion to type double (floating point promotion);

    if there�s any value of type double in the expression,  the other data will be converted to a double;

    if there�s any value of type long int in the expression, the other data will be converted to long int;
*/