#include <iostream>

class LIFO {
private:
	class Item {
	public:
		Item* left, * right;
		int value;
		Item(int val) : value(val) {
			left = right = nullptr;
		}
	};

	int _size;
	Item* first;
	Item* last;

private:
	Item* get_new_item(int val, Item* left = nullptr, Item* right = nullptr) {
		Item* item = new Item(val);
		item->left = left;
		item->right = right;
		return item;
	}

public:
	LIFO(std::initializer_list<int> list) {
		for (auto x = list.begin(); x != list.end(); x++) {
			this->push(*x);
		}
	}

	LIFO() {
		first = last = nullptr;
	}

	int pop() {
		if (!empty()) {
			int val;
			if (last != first) {
				auto temp = last->left;
				val = last->value;
				delete last;
				last = temp;
				last->right = nullptr;
				_size--;
				return val;
			}
			else {
				if (first != nullptr) {
					val = first->value;
					delete first;
					first = last = nullptr;
					_size--;
					return val;
				}
			}
		}
		else
			throw("ERROR: Pop on empty array");
	}

	void push(int val) {
		if (empty())
			first = last = this->get_new_item(val);
		else
			last = this->get_new_item(val, last);

		_size++;
	}

	bool empty() {
		return first == nullptr ? true : false;
	}

	int size() {
		return _size;
	}
};

void fun() {
	static int var = 99;
	std::cout << "var = " << ++var << std::endl;
}

class Class {
public:
	static int Counter;
	Class(void) { ++Counter; };
	~Class(void) {
		--Counter;
		if (Counter == 0) std::cout << "Bye, bye!" << std::endl;
	};
	static void HowMany(void) { std::cout << Counter << " instances" << std::endl; }
};
int Class::Counter = 0;

class Element {
	int value;
public:
	Element(int val) {
		value = val; std::cout << "Element(" << val << ") constructed!" << std::endl;
	}
	int Get(void) { return value; }
	void Put(int val) { value = val; }
};
class Collection {
	Element el1, el2;
public:
	Collection(void) : el1(2), el2(1) {
		std::cout << "Collection constructed!" << std::endl;
	}
	int Get(int elno) { return elno == 1 ? el1.Get() : el2.Get(); }
	int Put(int elno, int val) { if (elno == 1) el1.Put(val); else el2.Put(val); }
};

int main()
{
	LIFO lifo = { 1, 2, 3 };
	while (!lifo.empty())
		std::cout << lifo.pop() << std::endl;


	for (int i = 0; i < 5; i++)
		fun();

	Class::HowMany();
	Class a, b;
	b.HowMany();
	Class c, d;
	d.HowMany();

	Collection coll;

	return 0;
}