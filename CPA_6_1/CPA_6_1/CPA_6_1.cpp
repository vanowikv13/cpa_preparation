#include <iostream>
using namespace std;
class Super {
protected:
	int storage;
public:
	Super(int stor = 0) : storage(stor) {
		std::cout << "super constructor" << std::endl;
	}
	void put(int val) { storage = val; }
	int get(void) { return storage; }
};

class SuperSpecial {
protected:
	int safe;
public:
	void insert(int val) { safe = val; }
	int takeout(void) { return safe; }
};

class Sub : private Super {
public:
	void print(void) {
		std::cout << "storage = " << get() << std::endl;
	}

	void set(int val) {
		this->put(val);
	}

	Sub(int val) : Super(val) {
		std::cout << "Sub contructor" << std::endl;
	}
};

class SubSub : public Super {
public:
	SubSub(int val = 222) : Super(val) {
		std::cout << "SubSub constructor" << std::endl;
	}
};

class SubBoth : public Super, public SuperSpecial {
public:
	SubBoth() : Super() {

	}
	void print(void) {
		cout << "storage = " << storage << endl;
		cout << "safe    = " << safe << endl;
	}
};

int main(void) {
	Sub object(12);
	SubSub subsub;
	subsub.put(12);
	object.set(12);
	object.print();

	SubBoth both;
	both.put(1);	both.insert(2);
	both.put(both.get() + both.takeout());
	both.insert(both.get() + both.takeout());
	both.print();
	return 0;
}