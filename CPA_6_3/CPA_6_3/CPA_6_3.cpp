#include <iostream>
using namespace std;
class Pet {
protected:
	string name;
public:
	Pet(string n) {
		name = n;
		//this will take origin method cause not child object exist, parent constructor are called first
		MakeSound();
	}

	Pet() {}

	virtual void MakeSound(void) { cout << name << " the Pet says: Shh! Shh!" << endl; }

	void WakeUp(void) { 
		//this method will take override method not form Pet if it's possible
		MakeSound();
	}

	void setName(string name) { this->name = name; }
};
class Cat : public Pet {
public:
	Cat(string n) : Pet(n) { }
	void MakeSound(void) { cout << name << " the Cat says: Meow! Meow!" << endl; }
};
class Dog : public Pet {
public:
	Dog(string n) : Pet(n) { }
	void MakeSound(void) { cout << name << " the Dog says: Woof! Woof!" << endl; }
};

void PlayWithPetByValue(string name, Pet pet) {
	pet.setName(name);
	pet.MakeSound();
}

void PlayWithPetByPointer(string name, Pet* pet) {
	pet->setName(name);
	pet->MakeSound();
}

void PlayWithPetByReference(string name, Pet& pet) {
	pet.setName(name);
	pet.MakeSound();
}

int main(void) {
	Cat* a_cat;
	Dog* a_dog;
	Pet* p1 = new Pet(), p2;

	Pet** p3 = &p1;

	a_cat = new Cat("Kitty");
	a_cat->WakeUp();
	a_dog = new Dog("Doggie");
	a_dog->WakeUp();

	PlayWithPetByPointer("anonymous", p1);
	PlayWithPetByReference("no_name", p2);
	PlayWithPetByPointer("no_name", &p2);
	PlayWithPetByReference("anonymous", *p1);

	return 0;
}