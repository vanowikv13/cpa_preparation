#include <iostream>
using namespace std;
class A {
public:
	A(A& src) { cout << "copying A..." << endl; }
	A(void) { }
	void Do(void) { cout << "A is doing something" << endl; }
};
class B {
public:
	B(B& src) { cout << "copying B..." << endl; }
	B(void) { }
	void Do(void) { cout << "B is doing something" << endl; }
};
class Compo {
public:
	Compo(Compo& src) { cout << "Copying Compo..." << endl; }
	Compo(void) { };
	A f1;
	B f2;
};

class Class {
public:
	int field;
	Class(int n) : field(n) { };
	Class(Class& c) : field(0) { };
	Class(void) : field(1) { };
	void set(int n) { field = n; }
	int get(void) const { return field; }
};

void compUse(Compo comp) {
	std::cout << "using comp" << std::endl;
}

int main(void) {
	Compo  co1;
	//compUse(co1);
	Compo co2(co1);
	co2.f1.Do();
	co2.f2.Do();

	//6.6
	const Compo* ptr = &co1; //we can't edit this value
	//ptr->f1 = A(); //ERROR
	Compo* const ptr1 = &co1; //we can edit value, but we can't redirect pointer
	ptr1->f1 = A();
	//ptr1 = &co2; //ERROR
	const Compo* const ptr3 = &co1; //we can't edit value and we can't redirect
	//ptr3->f1 = A();
	//ptr3 = &co2; //ERROR

	const Class c(13);
	std::cout << c.get() << std::endl;
	return 0;
}