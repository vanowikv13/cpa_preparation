#include <iostream>
using namespace std;

class Friend;
class OtherClass;
class Class {
	friend class Friend;
	friend void setField(Class& c, int val);
private:
	int field;
	void print(void) { cout << "It's a secret, that field = " << field << endl; }
};

void setField(Class& c, int val) {
	c.field = val;
}

//class with firend declaration can have access to any field from class
//friendship isn't inherited
class Friend {
public:
	void DoIt(Class& c) {
		c.field = 100; c.print();
	}
	void DoIt(OtherClass& c);

	void DoIt(OtherClass& c, float val);
};

class OtherClass : public Class {
private:
	friend void Friend::DoIt(OtherClass& c, float val);
	//friend class Friend;
	float val;
};

void Friend::DoIt(OtherClass& c, float val) {
	c.val = val;
}

void Friend::DoIt(OtherClass& c)
{
	   c.field = 13;
	   c.print();
	   //c.val = 13; //friendship isn't inherited
}


int main(void) {
	Class o;
	Friend f;
	OtherClass oc;

	f.DoIt(o);
	f.DoIt(oc);
	setField(o, 123);
	return 0;
}