#include <iostream>
#include <string>

using namespace std;
class Class {
public:
	string msg;
	Class(string txt) : msg(txt) {}
};
void function(void) throw () {
	throw string("object");
}
void lastchance(void) {
	cout << "See what you've done! You've thrown an illegal exception!" << endl;
}

class A {
public:
	 virtual void f(void) throw(int, bad_exception) {
		 throw 3.14;
	 }
};

class AA : public A {
public:
	void aa(void) {};
};


class B {
public:
	//in explicit conversion to any type is denied
	explicit B(int) {}
};

void unexp(void) {
	cout << "Unexpected exception arrived!" << endl;
	throw;
}

int main(void) {
	set_unexpected(lastchance);
	try {
		function();
	}
	catch (string & exc) {
		cout << "Caught!" << endl;
	}

	//B b = 1; error
	B b(2); //good way

	A a;
	try {
		dynamic_cast<AA&>(a).aa();
	}
	catch (exception ex) {
		cout << "[" << ex.what() << "]" << endl;
	}

	set_unexpected(unexp);
	try {
		a.f();
	}
	catch (double f) {
		cout << "get Double" << endl;
	}
	catch (bad_exception err) {
		cout << "bad exception" << endl;
	}

	return 0;
}